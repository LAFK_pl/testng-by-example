package pl.lafk.examples.tng;

/**
 * Points to test directory.
 *
 * @author Tomasz @LAFK_pl Borek
 */
class App {
    public static void main(String[] args) {
        System.out.println("This app is just here to tell you to go see the tests. If you're launching this from a JAR, go to source repository and see the test directory instead.");
    }
}

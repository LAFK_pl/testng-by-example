package pl.lafk.examples.tng.params;

/**
 * @author Tomasz @LAFK_pl Borek
 */
class FizzBuzz {
    static String forNumber(int nr) {
        return String.valueOf(nr);
    }
}

package pl.lafk.examples.tng.params;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Iterator;
import java.util.Random;
import java.util.stream.IntStream;

import static org.testng.Assert.assertTrue;

/**
 * How random is random? Testing impact of parametrized tests on default settings.
 *
 * @see https://dzone.com/articles/why-many-java-performance-test
 * @author Tomasz @LAFK_pl Borek
 */
public class ImpactCheck {

    //TODO: Math to calculate when 100_000 won't end up with java.lang.OutOfMemoryError: Java heap space
    private static int impact = 10;

    @DataProvider
    public static Iterator<Integer> big() {
        return IntStream.rangeClosed(-impact, impact).iterator();
    }

    @Test(dataProvider = "big")
    public void howRandomIsRandom(int i) {
        assertTrue(i == new Random().nextInt(200) + 1 - 100);
    }

    //TODO: similar test case and math for an array-based solution

}

package pl.lafk.examples.tng.params;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

/**
 * Demonstrates DataProvider on a FizzBuzz kata
 *
 * @author Tomasz @LAFK_pl Borek
 */
public class FizzBuzzTest {

    @DataProvider
    public static Object[][] allNumbersTillAllCasesAreCovered() {
        return new Object[][] {
            {1, "1"}
            ,{2, "2"}
            ,{3, "Fizz"}
            ,{4, "4"}
            ,{5, "Buzz"}
            ,{6, "Fizz"}
            ,{7, "7"}
            ,{8, "8"}
            ,{9, "Fizz"}
            ,{10, "Buzz"}
            ,{11, "11"}
            ,{12, "Fizz"}
            ,{13, "13"}
            ,{14, "14"}
            ,{15, "FizzBuzz"}
        };
    }


    @Test(dataProvider = "allNumbersTillAllCasesAreCovered")
    public void fizzBuzzNormalNumberTest(int nr, String result) {
        assertEquals(FizzBuzz.forNumber(nr), result,
                String.format("for %d we got %s, Fizz should be if divisable by 3 and Buzz if by 5, both if by both, otherwise input is output", nr, result));
    }
}

package pl.lafk.examples.tng.params;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

/**
 * Demonstrates DataProvider on a Roman Numerals kata
 *
 * @author Tomasz @LAFK_pl Borek
 */
public class RomanNumeralsTest {

    @DataProvider
    public static Object[][] allNumbersFromFizzBuzz() {
        return new Object[][] {
            {1, "I"}
            ,{2, "II"}
            ,{3, "III"}
            ,{4, "IV"}
            ,{5, "V"}
            ,{6, "VI"}
            ,{7, "VII"}
            ,{8, "VIII"}
            ,{9, "IX"}
            ,{10, "X"}
            ,{11, "XI"}
            ,{12, "XII"}
            ,{13, "XIII"}
            ,{14, "XIV"}
            ,{15, "XV"}
        };
    }


    @Test(dataProvider = "allNumbersFromFizzBuzz")
    public void fizzBuzzNormalNumberTest(int arabicNr, String result) {
        assertEquals(Romans.from(arabicNr), result,
                String.format("for %d we got %s, 1 is I, 5 is V, 10 is X", arabicNr, result));
    }
}

package pl.lafk.examples.tng.results;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * 3 ways in TestNG to test exceptions.
 *
 * @author Tomasz @LAFK_pl Borek
 * @see TestingTraps for previous
 * @see pl.lafk.examples.tng.params next
 */
@Test
public class TestExceptions {

    @Test
    public void oldStyle() { //TODO: why this fails?
        // given
        String crashCause = "nothing was thrown";
        ThisThrows exceptionalSUT = new ThisThrows();
        Exception expected = new Exception();

        // when
        try {
            exceptionalSUT.throwNow(expected);
        } catch (Exception e) {
            // then
            Assert.assertEquals(e.getClass(), expected.getClass(), "not the class I was looking for!");
//            return;
        }
        Assert.fail(crashCause);
    }

    @Test(expectedExceptions = Exception.class)
    public void newStyle() throws Exception {
        // given
        ThisThrows exceptionalSUT = new ThisThrows();
        Exception expected = new Exception();

        // when
        exceptionalSUT.throwNow(expected);
    }

}

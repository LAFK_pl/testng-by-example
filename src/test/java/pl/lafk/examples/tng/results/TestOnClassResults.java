package pl.lafk.examples.tng.results;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.Test;

/**
 * With @Test on a class how do we get all results possible?
 *
 * @author Tomasz @LAFK_pl Borek
 * @see pl.lafk.examples.tng.assertions for previous
 * @see TestOnMethodsResults next
 */
@Test
public class TestOnClassResults {

    public void aSuccess() {
        Assert.assertEquals(1, 1);
    }
    public void failsOnAssert() {
        Assert.assertEquals(1, 2);
    }
    public void failsInTest() {
        // Runtime error
        Assert.assertTrue(2/0 == 1);
    }

    void unseenNotIgnored() {
        Assert.assertEquals(1, 1);
    }

    public void skippedMeansIgnored() {
        throw new SkipException("ignored despite class being annotated");
    }


    public void aSuccessDespiteReturn() {
        Assert.assertEquals(1, 1);
        return;
    }
    public String triangleLiesCauseVoidButString() {
        Assert.assertEquals(1, 1);
        return "String is special";
    }
    public Integer triangleLiesCauseNotVoidAndInteger() {
        Assert.assertEquals(1, 1);
        return Integer.valueOf(1);
    }

}

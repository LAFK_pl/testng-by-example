package pl.lafk.examples.tng.results;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.Test;

/**
 * With @Test on a method how do we get all results possible?
 *
 * @author Tomasz @LAFK_pl Borek
 * @see TestOnClassResults for previous
 * @see TrickyThrowingTest next
 */
public class TestOnMethodsResults {

    @Test
    public void aSuccess() {
        Assert.assertEquals(1, 1);
    }
    @Test
    public void failsOnAssert() {
        Assert.assertEquals(1, 2);
    }
    @Test
    public void failsInTest() {
        // Runtime error
        Assert.assertTrue(2/0 == 1);
    }

    @Test(enabled = false)
    public void turnedOffNotIgnored() {
        Assert.assertEquals(1, 1);
    }
    @Test
    public void skippedMeansIgnored() {
        throw new SkipException("ignored despite being annotated");
    }


    @Test
    public void tricky() {
            throw new AssertionError("what throws this error?");
        }
    @Test
    public void alsoTricky() throws Throwable {
        throw new Throwable("what about throwing throwable?");
    }

}

package pl.lafk.examples.tng.results;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Two basic traps in testing
 *
 * @author Tomasz @LAFK_pl Borek
 * @see TrickyThrowingTest for previous
 * @see TestingTestNGTemplates next
 */
@Test
public class TestingTraps {

    @Test
    public void testCaseToBeFilledOut_Trap1() {
        // what will be the result?
    }

    @Test
    public void testingTheExceptionHandling_Trap2() {
        // TODO: good exception code
    }

}

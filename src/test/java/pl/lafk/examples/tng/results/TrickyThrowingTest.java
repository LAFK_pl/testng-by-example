package pl.lafk.examples.tng.results;

import org.testng.annotations.Test;

/**
 * With @Test on a method how do we get all results possible?
 *
 * @author Tomasz @LAFK_pl Borek
 * @see TestOnMethodsResults for previous
 * @see TestingTraps next
 */
@Test
public class TrickyThrowingTest {

    @Test
    public void trickyMethod() {
            throw new AssertionError("what throws this error?");
        }
    @Test
    public void alsoTrickyMethod() throws Throwable {
        throw new Throwable("what about throwing throwable?");
    }

    public void tricky() {
        throw new AssertionError("what throws this error?");
    }

    public void trickyAsWell() throws Throwable {
        throw new Throwable("whatever can be thrown, may be thrown");
    }

    // TODO: how does Assert.fail work and why we have it?
}

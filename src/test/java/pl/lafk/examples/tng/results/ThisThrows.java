package pl.lafk.examples.tng.results;

/**
 * @author Tomasz @LAFK_pl Borek
 */
public class ThisThrows {
    public void throwNow(Exception expected) throws Exception {
        throw expected;
    }
}

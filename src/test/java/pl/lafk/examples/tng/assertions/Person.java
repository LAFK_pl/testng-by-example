package pl.lafk.examples.tng.assertions;

/**
 * For the sake of soft assertion and the great printing imp.
 *
 * And awesome humour Stanisław Lem had shown then.
 *
 * @author Tomasz @LAFK_pl Borek
 */
class Person {
    private String name;
    private String surname;
    private int age;
    private String job;

    Person(String job, String name, String surname, int age) {

        this.job = job;
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    Person() {}

    String getName() {
        return this.name;
    }

    String getSurname() {
        return this.surname;
    }

    String getJob() {
        return this.job;
    }

    int getAge() {
        return this.age;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s age %d", job, name, surname, age);
    }
}

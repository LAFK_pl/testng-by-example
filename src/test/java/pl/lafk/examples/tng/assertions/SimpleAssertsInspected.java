package pl.lafk.examples.tng.assertions;

import java.util.ArrayList;
import java.util.Arrays;

import static org.testng.Assert.*;

/**
 * Intellij (Ultimate?) has a nice feature called inspections.
 * Here we show how to use it to learn about TestNG basic assertions and adjust TestNG to our needs.
 *
 * @author Tomasz @LAFK_pl Borek
 * @see HardAssertsSoftAsserts for previous
 * @see pl.lafk.examples.tng.results next
 */
class SimpleAssertsInspected {

    public void staticImportsRulez() {
        assertEquals(1, 1);
        ArrayList a;
        assertEquals(1==1, true); // true
        assertTrue(1 == 1, "1 != 1 since when?!"); // equals

        assertNotEquals(1==1, false);
        assertFalse(true == false); // notEquals

        assertFalse(true); // fail
    }

}
